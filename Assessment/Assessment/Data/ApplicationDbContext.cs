﻿using Assessment.Models;
using Microsoft.EntityFrameworkCore;

namespace Assessment.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Claim>? Claims { get; set; }
        public DbSet<Owner>? Owners { get; set; }
        public DbSet<Vehicle>? Vehicles { get; set; }
    }

}
