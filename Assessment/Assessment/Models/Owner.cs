﻿using Newtonsoft.Json;

namespace Assessment.Models
{
    public class Owner
    {
        public int Id { get; set; }
        public String FirstName { get; set; } = "";
        public String LastName { get; set; }
        public String DriverLicense { get; set; } = "";

        [JsonIgnore]
        public List<Vehicle>? Vehicles { get; set; }

    }
}
