﻿using Newtonsoft.Json;

namespace Assessment.Models
{
    public class Vehicle
    {
        public int Id { get; set; }
        public String Brand { get; set; } = "";
        public String Vin { get; set; } = "";
        public int Year { get; set; }
        public int OwnerId { get; set; }

        [JsonIgnore]
        public Owner? Owner { get; set; }

        [JsonIgnore]
        public List<Claim>? Claims { get; set; }

    }
}
