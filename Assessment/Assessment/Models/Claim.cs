﻿using System.Text.Json.Serialization;

namespace Assessment.Models
{
    public class Claim
    {
        public int Id { get; set; }
        public String Description { get; set; } = "";
        public String Status { get; set; } = "";
        public DateTime Date { get; set; }
        public int VehicleId { get; set; }

        [JsonIgnore]
        public Vehicle? Vehicle { get; set; }
       
    }
}
